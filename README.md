# Streaming server experiment

## Instructions

1. `cp .env.example .env`
2. Add your Anthropic API key to `.env`
3. `make run`

### Test streaming with fake Model Gateway endpoint

```
curl --request POST --url http://0.0.0.0:5052/v2/code/completions --no-buffer
```

### Test streaming with fake Workhorse + fake Model Gateway

```
curl --request POST --url http://0.0.0.0:5052/fake-workhorse --no-buffer
```

### Test streaming with (fake Workhorse + fake Model Gateway) and pretty print the result

```
curl --request POST --url http://0.0.0.0:5052/pretty-print --no-buffer
```
