package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/joho/godotenv"
)

const (
	ioCopySolution        = false
	prompt         string = `


Human: Generate the code in Golang to create a new calculator type
the type should have the following methods:
add, sub, multiply, divide, sqrt, abs, sin, cos


Assistant:
`
)

type AnthropicResponse struct {
	Completion string `json:"completion"`
}

func main() {
	godotenv.Load()

	fmt.Println("Starting stream server...")

	http.HandleFunc("/v2/code/completions", codeCompletionHandler)
	http.HandleFunc("/fake-workhorse", fakeWorkhorseHandler)
	http.HandleFunc("/pretty-print", prettyPrintHandler)

	http.ListenAndServe(":5052", nil)
}

func codeCompletionHandler(w http.ResponseWriter, r *http.Request) {
	client := &http.Client{}
	data := map[string]interface{}{
		"model":                "claude-2",
		"prompt":               prompt,
		"max_tokens_to_sample": 256,
		"stream":               true,
	}
	jsonData, err := json.Marshal(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	req, err := http.NewRequest("POST", "https://api.anthropic.com/v1/complete", bytes.NewBuffer(jsonData))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	req.Header.Set("anthropic-version", "2023-06-01")
	req.Header.Set("content-type", "application/json")
	req.Header.Set("x-api-key", os.Getenv("ANTHROPIC_API_KEY"))

	resp, err := client.Do(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	w.Header().Set("Content-Type", "text/event-stream")

	if ioCopySolution {
		w.Header().Set("Transfer-Encoding", "chunked")
		if _, err := io.Copy(w, resp.Body); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	} else {
		scanner := bufio.NewScanner(resp.Body)
		for scanner.Scan() {
			line := scanner.Text()
			fmt.Fprintln(w, line)
			if flusher, ok := w.(http.Flusher); ok {
				flusher.Flush()
			}
		}
		if err := scanner.Err(); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

func fakeWorkhorseHandler(w http.ResponseWriter, r *http.Request) {
	client := &http.Client{
		Transport: &http.Transport{
			DisableCompression: true,
		},
	}
	req, err := http.NewRequest("GET", "http://localhost:5052/v2/code/completions", nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp, err := client.Do(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	// Current Workhorse implementation which buffers some response
	// if _, err := io.Copy(w, resp.Body); err != nil {
	// 	http.Error(w, err.Error(), http.StatusInternalServerError)
	// 	return
	// }

	// Experimental implementaion that streams in smaller chunks
	scanner := bufio.NewScanner(resp.Body)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Fprintln(w, line)
		if flusher, ok := w.(http.Flusher); ok {
			flusher.Flush()
		}
	}
	if err := scanner.Err(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func prettyPrintHandler(w http.ResponseWriter, r *http.Request) {
	client := &http.Client{
		Transport: &http.Transport{
			DisableCompression: true,
		},
	}
	req, err := http.NewRequest("GET", "http://localhost:5052/fake-workhorse", nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp, err := client.Do(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	scanner := bufio.NewScanner(resp.Body)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "data: ") {
			c := line[6:]
			ar := AnthropicResponse{}
			json.Unmarshal([]byte(c), &ar)

			fmt.Fprint(w, ar.Completion)
			if flusher, ok := w.(http.Flusher); ok {
				flusher.Flush()
			}
		}
	}
	if err := scanner.Err(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
